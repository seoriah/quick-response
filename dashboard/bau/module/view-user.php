<?php 
if ($_GET['page']='view-user'){
?>
            <div class="row">
                <div class="col-lg-12">
					<h3 class="page-header"><strong>Data Pengguna</strong></h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                       
                            <a class="btn btn-info btn-sm" href="index.php?page=input-user">TAMBAH</a>
                            <a class="btn btn-info btn-sm" href="export.php?act=exp-user">PRINT</a>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                        <th class="text-center">NO</th>
                                            <th class="text-center">NIM</th>
                                           <th class="text-center">NAMA</th>
                                            <th class="text-center">PASSWORD</th>
                                            <th class="text-center">
                                            EMAIL</th>
                                           <th class="text-center">
                                            AKSI</th>
                                        </tr>
                                    </thead>
                                    <tbody>
<?php
    $no=1;
	$sql=mysqli_query($conn,"SELECT * FROM pengguna ORDER BY nim ASC");
    
	while($rs=mysqli_fetch_assoc($sql)){

?>                            

                            <tr class="odd gradeX">
                                            <td class="text-center"><?php echo $no; ?></td>
                                            <td ><?php echo"$rs[nim]";  ?></td>
                                            <td ><?php echo"$rs[nama]";  ?></td>
                                            <td ><?php echo"$rs[password]";  ?></td>
                                            <td ><?php echo"$rs[email]";  ?></td>
                                             <td class="text-center"><a href="index.php?page=update-user&nim=<?php echo $rs['nim'] ?>"><button type="button" class="btn btn-warning btn-xs" >Edit</button> 
                                             <a href="index.php?page=proses&act=delete-user&nim=<?php echo $rs['nim'] ?>"><button type="button" class="btn btn-danger btn-xs" >Hapus</button> 
                                        </tr>
                                        
<?php
$no++;
    
    }

?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
<?php
}
?>