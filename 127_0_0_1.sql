-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 16, 2016 at 06:05 PM
-- Server version: 5.7.11
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qrespone`
--
CREATE DATABASE IF NOT EXISTS `qrespone` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `qrespone`;

-- --------------------------------------------------------

--
-- Table structure for table `pengaduan_bau`
--

CREATE TABLE `pengaduan_bau` (
  `id_pengaduan` int(255) NOT NULL,
  `nim` int(10) NOT NULL,
  `pesan` text NOT NULL,
  `konfirmasi` text NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengaduan_lab`
--

CREATE TABLE `pengaduan_lab` (
  `id_pengaduan` int(255) NOT NULL,
  `nim` int(10) NOT NULL,
  `pesan` text NOT NULL,
  `konfirmasi` text NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `nim` int(10) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `password` text NOT NULL,
  `email` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`nim`, `nama`, `password`, `email`) VALUES
(12012792, 'HAIRUS ZAMAN', 'sandihairoes', 'hairoes@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id_user` int(10) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` text NOT NULL,
  `level_user` varchar(15) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `nama`, `username`, `password`, `level_user`) VALUES
(1, 'Bagian Administrasi Umum', 'jasri', 'jasri', 'bau'),
(2, 'Laboratorium', 'zainal', 'zainal', 'lab'),
(3, 'Administrator', 'admin', 'admin', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pengaduan_bau`
--
ALTER TABLE `pengaduan_bau`
  ADD PRIMARY KEY (`id_pengaduan`);

--
-- Indexes for table `pengaduan_lab`
--
ALTER TABLE `pengaduan_lab`
  ADD PRIMARY KEY (`id_pengaduan`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`nim`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pengaduan_bau`
--
ALTER TABLE `pengaduan_bau`
  MODIFY `id_pengaduan` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `pengaduan_lab`
--
ALTER TABLE `pengaduan_lab`
  MODIFY `id_pengaduan` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
